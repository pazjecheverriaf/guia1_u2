import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk



class MainWindow():
    
    def __init__(self):
        

        Builder = Gtk.Builder()
        Builder.add_from_file("./ui/intento.ui")

        window = Builder.get_object("borrador")
        window.connect("destroy", Gtk.main_quit)
        window.set_title("ventana de texo ")
        window.resize(800, 600)
        

        #botones
        boton_aceptar= Builder.get_object("btn_aceptar")
        boton_aceptar.connect("clicked", self.btn_aceptar)
      #  boton_aceptar.connect("clicked", self.on_btn_Aceptar)
                                                
        boton_reset = Builder.get_object("btn_reset")
        boton_reset.connect("clicked", self.on_btn_reset)

        boton_abrir_dialogo = Builder.get_object("btn_abrir_dlg")
        boton_abrir_dialogo.connect("clicked", self.btn_abrir_dlg)

        


        #texto 
        self.texto = Builder.get_object("texto1")
        self.texto = Builder.get_object("texto2")

        window.show_all()


    def on_btn_aceptar_click(self, btn=None):
        texto = self.label.texto.get_text()
        self.label_borrador.set_text(texto)
        self.texto.set_text("")


    def on_btn_reset(self, btn=None):
        texto = self.texto.get_text()
        self.label_exaple.set_text(texto)
        self.texto.set_text("")


    def on_btn_abrir_dlg(self, btn=None):
        print("click aqui")
        dlg = DlgMain()
        response = dlg.dialogo.run()

        if response == Gtk.responseType.CANCEL:
            print("presione el boton cancelar ")



if __name__ == "__main__":
    MainWindow()
    Gtk.main()

